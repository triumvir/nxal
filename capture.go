package nxal

// Capture is used to store the data parsed from a log line.
type Capture struct {
	BodyBytesSent          string
	BytesSent              string
	Connection             string
	HTTPReferer            string
	HTTPUserAgent          string
	Msec                   string
	Pipe                   string
	RemoteAddr             string
	RemoteUser             string
	Request                string
	RequestLength          string
	RequestMethod          string
	RequestProtocol        string
	RequestProtocolName    string
	RequestProtocolVersion string
	RequestTime            string
	RequestURI             string
	Status                 string
	TimeISO8601            string
	TimeLocal              string
	UpstreamConnectTime    string
	UpstreamHeaderTime     string
	UpstreamResponseTime   string
}
