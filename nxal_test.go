package nxal

import (
	"fmt"
	"reflect"
	"regexp"
	"testing"
)

func TestCompile(t *testing.T) {
	t.Run("default", func(t *testing.T) {
		p := Parser{}
		err := p.Compile(DefaultLogFormat)
		if err != nil {
			t.Errorf("compiling regexp failed: %s", err.Error())
		}
		if got, want := p.String(), DefaultLogFormat; got != want {
			t.Errorf("parsing string failed:\ngot:  %v\nwant: %v", got, want)
		}
	})

	t.Run("custom", func(t *testing.T) {
		p := Parser{}
		err := p.Compile(Pattern.RemoteAddr)
		if err != nil {
			t.Errorf("compiling regexp failed: %s", err.Error())
		}
		if got, want := p.String(), Pattern.RemoteAddr; got != want {
			t.Errorf("compiling regexp failed:\ngot:  %v\nwant: %v", got, want)
		}
	})

	t.Run("empty", func(t *testing.T) {
		p := Parser{}
		if got, want := p.String(), ""; got != want {
			t.Errorf("parsing string failed:\ngot:  %v\nwant: %v", got, want)
		}
		err := p.Compile("")
		if err != nil {
			t.Errorf("compiling regexp failed: %s", err.Error())
		}
		if got, want := p.String(), ""; got != want {
			t.Errorf("parsing string failed:\ngot:  %v\nwant: %v", got, want)
		}
	})

	t.Run("broken", func(t *testing.T) {
		p := Parser{}
		err := p.Compile("[[")
		if err == nil {
			t.Errorf("compiling regexp failed: no error on broken regexp")
		}
	})
}

func TestParse(t *testing.T) {
	t.Run("default", func(t *testing.T) {
		c := Capture{}
		b := []byte(`127.0.0.1 - - [01/Mar/2019:09:52:03 +0000] "GET / HTTP/1.1" 301 178 "-" "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36"`)
		p := Parser{}
		p.Compile(DefaultLogFormat)
		p.Parse(b, &c)
		f := fmt.Sprintf(
			`%s - %s [%s] "%s" %s %s "%s" "%s"`,
			c.RemoteAddr, c.RemoteUser, c.TimeLocal, c.Request, c.Status, c.BodyBytesSent, c.HTTPReferer, c.HTTPUserAgent)
		if got, want := f, string(b); got != want {
			t.Errorf("parsing failed:\ngot:  %v\nwant: %v", got, want)
		}
	})

	t.Run("upstream", func(t *testing.T) {
		c := Capture{}
		b := []byte(`12.010 5.200 0.001`)
		p := Parser{}
		p.Compile(fmt.Sprintf(
			`%s %s %s`,
			Pattern.UpstreamConnectTime, Pattern.UpstreamHeaderTime, Pattern.UpstreamResponseTime))
		p.Parse(b, &c)
		f := fmt.Sprintf(
			`%s %s %s`,
			c.UpstreamConnectTime, c.UpstreamHeaderTime, c.UpstreamResponseTime)
		if got, want := f, string(b); got != want {
			t.Errorf("parsing failed:\ngot:  %v\nwant: %v", got, want)
		}
	})

	t.Run("broken", func(t *testing.T) {
		c := Capture{}
		b := []byte(`127.0.0.1 - [01/Mar/2019:09:52:03 +0000] "GET / HTTP/1.1" 301 178 "-" "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36"`)
		p := Parser{}
		p.Compile(DefaultLogFormat)
		if p.Parse(b, &c) {
			t.Error("parsing failed: default regexp parse true on broken string")
		}
	})

	t.Run("nil", func(t *testing.T) {
		c := Capture{}
		b := []byte(`127.0.0.1 - - [01/Mar/2019:09:52:03 +0000] "GET / HTTP/1.1" 301 178 "-" "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36"`)
		p := Parser{}
		if p.Parse(b, &c) {
			t.Error("parsing failed: nil regexp parse true")
		}
	})
}

func TestPattern(t *testing.T) {
	t.Run("patterns", func(t *testing.T) {
		rv := reflect.Indirect(reflect.ValueOf(&Pattern))
		for i := 0; i < rv.NumField(); i++ {
			n := rv.Type().Field(i).Name
			p := rv.Field(i).Interface().(string)
			_, err := regexp.Compile(p)
			if err != nil {
				t.Errorf("compiling regexp failed:\nname:    %s\npattern: %s\nerror:   %s",
					n, p, err)
			}
		}
	})

	t.Run("default-log-format", func(t *testing.T) {
		_, err := regexp.Compile(DefaultLogFormat)
		if err != nil {
			t.Errorf("compiling regexp failed:\nname:    %s\npattern: %s\nerror:   %s",
				"DefaultLogFormat", DefaultLogFormat, err)
		}
	})
}
