nxal
====

[![godoc](http://img.shields.io/badge/godoc-reference-blue.svg?style=flat)](https://godoc.org/gitlab.com/triumvir/nxal) [![License MIT](https://img.shields.io/badge/License-MIT-red.svg?style=flat)](https://gitlab.com/triumvir/nxal/blob/master/LICENSE) [![Build Status](https://gitlab.com/triumvir/nxal/badges/master/build.svg)](https://gitlab.com/triumvir/nxal/commits/master) [![Coverage Report](https://gitlab.com/triumvir/nxal/badges/master/coverage.svg)](https://gitlab.com/triumvir/nxal/commits/master) [![Coverage Report](https://goreportcard.com/badge/gitlab.com/triumvir/nxal)](https://goreportcard.com/report/gitlab.com/triumvir/nxal)

Nginx access log parser.

Easily parse nginx access logs in the default or any custom format.


Installation
------------

```
go get -u gitlab.com/triumvir/nxal
```


Getting Started
---------------

### Default Access Log Format

Parsing access logs in the default nginx "combined" format:

```go
package main

import (
    "fmt"
    "gitlab.com/triumvir/nxal"
)

func main() {
    re := nxal.Parser{}
    re.Compile(nxal.DefaultLogFormat)
    c := nxal.Capture{}
    re.Parse(
        []byte(`127.0.0.1 - - [01/Mar/2019:09:52:03 +0000] "GET / HTTP/1.1" 301 178 "-" "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36"`),
        &c)
    fmt.Println(c.Status)
}

// Output: 301
```


### Default Access Log Format With Appended Fields

If the access log is in the default nginx format but with added fields:

```go
package main

import (
    "fmt"
    "gitlab.com/triumvir/nxal"
)

func main() {
    re := nxal.Parser{}
    re.Compile(fmt.Sprintf(
        "%s %s", nxal.DefaultLogFormat, nxal.Pattern.UpstreamResponseTime)
    c := nxal.Capture{}
    re.Parse(
        []byte(`127.0.0.1 - - [01/Mar/2019:09:52:03 +0000] "GET / HTTP/1.1" 301 178 "-" "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36" 0.141`),
        &c)
    fmt.Println(c.UpstreamResponseTime)
}

// Output: 0.141
```


Patterns and Captures
---------------------

The Pattern struct contains regular expression patterns each matching an nginx
variable. The matches are captured into a Captures struct, where the field names
correspond to the field names in Pattern.

* BodyBytesSent
* BytesSent
* Connection
* HTTPReferer
* HTTPUserAgent
* Msec
* Pipe
* RemoteAddr
* RemoteUser
* Request (also captures parts into RequestMethod, RequestProtocol, RequestProtocolName, RequestProtocolVersion, RequestURI)
* RequestLength
* RequestTime
* Status
* TimeISO8601
* TimeLocal
* UpstreamConnectTime
* UpstreamHeaderTime
* UpstreamResponseTime


Caveats
-------

The log format pattern you compile should match the complete log format, or
at the very least enough to guarantee a proper match. The patterns defined in
Pattern are very simple and only useful when properly combined.
