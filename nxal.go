// Package nxal provides easy parsing of nginx access logs.
//
// Parsing lines in the default nginx "combined" format is very easy:
//
//     re := nxal.Parser{}
//     re.Compile(nxal.DefaultLogFormat)
//     c := nxal.Capture{}
//     re.Parse(
//         []byte(`127.0.0.1 - - [01/Mar/2019:09:52:03 +0000] "GET / HTTP/1.1" 301 178 "-" "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36"`),
//         &c)
//     fmt.Println(c.Status)
//     // Output: 301
//
// Parsing a custom format is also easy:
//
//     re.Compile(fmt.Sprintf(
//         "%s %s", nxal.DefaultLogFormat, nxal.Pattern.UpstreamResponseTime))
//     re.Parse(
//         []byte(`127.0.0.1 - - [01/Mar/2019:09:52:03 +0000] "GET / HTTP/1.1" 301 178 "-" "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36"` 0.141),
//         &c)
//     fmt.Println(c.UpstreamResponseTime)
//     // Output: 0.141
//
// Pattern.Request captures $request as a whole, but it also
// stores the diffent components in their own fields:
//
//    fmt.Println(c.Request)
//    // Output: GET / HTTP/1.1
//    fmt.Printf("%s %s %s", c.RequestMethod, c.RequestURI, c.RequestProtocol)
//    // Output: GET / HTTP/1.1
//    fmt.Printf("%s/%s", c.RequestProtocolName, c.RequestProtocolVersion)
//    // Output: HTTP/1.1
//
// Caveats
//
// The log format pattern you compile should match the complete log format, or
// at the very least enough to guarantee a proper match. The patterns defined in
// Pattern are very simple and only useful when properly combined.
package nxal

import (
	"reflect"
	"regexp"
)

// A Parser holds a regular expression and parses access log lines.
type Parser struct {
	regexp *regexp.Regexp
}

// Parse a log line into a given Capture struct.
func (p *Parser) Parse(b []byte, c *Capture) bool {
	if p.regexp == nil {
		return false
	}
	names := p.regexp.SubexpNames()
	res := p.regexp.FindSubmatch(b)
	if len(res) == 0 {
		return false
	}
	rv := reflect.Indirect(reflect.ValueOf(c))
	for i, e := range res {
		if names[i] == "" || !rv.FieldByName(names[i]).CanSet() {
			continue
		}
		rv.FieldByName(names[i]).SetString(string(e))
	}
	return true
}

// Compile a new regexp pattern into the parser.
func (p *Parser) Compile(pt string) error {
	r, err := regexp.Compile(pt)
	if err != nil {
		return err
	}
	p.regexp = r
	return nil
}

// String returns the parsers regular expression pattern.
func (p *Parser) String() string {
	if p.regexp == nil {
		return ""
	}
	return p.regexp.String()
}
