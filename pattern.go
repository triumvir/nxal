package nxal

import "fmt"

type pattern struct {
	BodyBytesSent        string
	BytesSent            string
	Connection           string
	HTTPReferer          string
	HTTPUserAgent        string
	Msec                 string
	Pipe                 string
	RemoteAddr           string
	RemoteUser           string
	Request              string
	RequestLength        string
	RequestTime          string
	Status               string
	TimeISO8601          string
	TimeLocal            string
	UpstreamConnectTime  string
	UpstreamHeaderTime   string
	UpstreamResponseTime string
}

// Pattern contains regular expression patterns matching variables commonly used
// in logs.
var Pattern = pattern{
	BodyBytesSent:        `(?P<BodyBytesSent>\d+)`,
	BytesSent:            `(?P<BytesSent>\d+)`,
	Connection:           `(?P<Connection>\d+)`,
	HTTPReferer:          `(?P<HTTPReferer>.+)`,
	HTTPUserAgent:        `(?P<HTTPUserAgent>.+)`,
	Msec:                 `(?P<Msec>\d+\.\d{3})`,
	Pipe:                 `(?P<Pipe>[p\.])`,
	RemoteAddr:           `(?P<RemoteAddr>\d+.\d+.\d+.\d+)`,
	RemoteUser:           `(?P<RemoteUser>(\S)+)`,
	Request:              `(?P<Request>(?P<RequestMethod>\w+)\s(?P<RequestURI>\S+)\s(?P<RequestProtocol>(?P<RequestProtocolName>\w+)/(?P<RequestProtocolVersion>\S+)))`,
	RequestLength:        `(?P<RequestLength>\d+)`,
	RequestTime:          `(?P<RequestTime>\d+\.\d{3})`,
	Status:               `(?P<Status>\d+)`,
	TimeISO8601:          `(?P<TimeISO8601>\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}[+\-]\d{2}:\d{2})`,
	TimeLocal:            `(?P<TimeLocal>\d{2}\/[A-z]{3}\/\d{4}:\d{2}:\d{2}:\d{2} [+\-]\d{4})`,
	UpstreamConnectTime:  `(?P<UpstreamConnectTime>\d+\.\d{3}|-)`,
	UpstreamHeaderTime:   `(?P<UpstreamHeaderTime>\d+\.\d{3}|-)`,
	UpstreamResponseTime: `(?P<UpstreamResponseTime>\d+\.\d{3}|-)`}

// DefaultLogFormat is a regular expression pattern matching the nginx default
// "combined" log format.
var DefaultLogFormat = fmt.Sprintf(
	`%s - %s \[%s\] "%s" %s %s "%s" "%s"`,
	Pattern.RemoteAddr, Pattern.RemoteUser, Pattern.TimeLocal, Pattern.Request,
	Pattern.Status, Pattern.BodyBytesSent, Pattern.HTTPReferer, Pattern.HTTPUserAgent)
